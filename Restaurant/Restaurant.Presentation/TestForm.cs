﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Restaurant.Presentation
{
    public partial class TestForm : Form
    {

        private string _connectionString = "server=127.0.0.1;uid=root;pwd=root;database=restaurant";
        private MySqlConnection conn;
        public TestForm()
        {
            InitializeComponent();
        }

        void testBtn_Click(object sender, EventArgs e)
        {
            conn = new SqlConnection(_connectionString);
            try
            {
                conn.Open();
                MessageBox.Show("Connection Open ! ");
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! ");
            }
        }
    }
}
