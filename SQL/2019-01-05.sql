create schema Restaurant;
use Restaurant;

CREATE TABLE Customer (
customerID INT AUTO_INCREMENT PRIMARY KEY,
customerName VARCHAR(20),
customerLastName VARCHAR(20),
customerAddress VARCHAR(80),
customerPhone VARCHAR(12),
customerOrder INT,
FOREIGN KEY (customerOrder) REFERENCES _Order(orderId)
);

CREATE TABLE _Order(
orderID INT AUTO_INCREMENT PRIMARY KEY,
orderDate DATETIME,
requiredDate DATETIME,
shippedDate DATETIME,
orderStatus VARCHAR(15),
comments TEXT,
orderMeal INT,
orderPrice INT REFERENCES Meal(mealPrice),
FOREIGN KEY (orderMeal) REFERENCES Meal(mealId)
);

CREATE TABLE Meal (
mealId int AUTO_INCREMENT PRIMARY KEY,
mealDrink int,
mealFood int,
mealPrice int,
mealSnack int,
FOREIGN KEY (mealDrink) REFERENCES Drink(drinkId),
FOREIGN KEY (mealSnack) REFERENCES Snack(snackId),
FOREIGN KEY (mealFood) REFERENCES Food(foodId)
);

CREATE TABLE Drink (
drinkId INT AUTO_INCREMENT PRIMARY KEY,
drinkName varchar(20),
drinkPrice INT,
drinkQuantity INT
);

CREATE TABLE Snack(
snackId INT AUTO_INCREMENT PRIMARY KEY,
snackName varchar (20),
snackSauce int,
snackPrice INT,
snackQuantity INT,
FOREIGN KEY (snackSauce) REFERENCES Sauce(sauceId)
);

CREATE TABLE Sauce(
sauceId INT AUTO_INCREMENT PRIMARY KEY,
sauceName varchar (20),
sauceQuantity INT,
saucePrice INT
);

CREATE TABLE Food (
foodId INT AUTO_INCREMENT PRIMARY KEY,
foodName varchar (20),
foodPrice int,
foodIngredients int,
foodQuantity int,
FOREIGN KEY (foodIngredients) REFERENCES Ingredient(ingredientId)
);

CREATE TABLE Ingredient(
ingredientId INT AUTO_INCREMENT PRIMARY KEY,
ingredientName varchar(20),
ingredientQuantity int,
ingredientPrice int
);